/*
Module Dependencies 
*/
var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require("cookie-parser");
var app = express();


/* set the view engine */
app.set('view engine', 'ejs');
app.set('views', __dirname +'/views');


/*adding routes*/
var routes = require('./server/router/router');

/* bodyParser */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

/* cookieParser */
app.use(cookieParser());

// app.use(session({ secret: '$#%!@#@@#SSDASASDVV@@@@', key: 'sid'}));


/* You have to tell Express from where it should deliver 
static files. Static files are images, JavaScript library, 
CSS files etc. You can specify by using. */
/*adding static content*/
app.use(express.static(__dirname + '/public'));
/*-------------------------------------------------------*/

app.use('/', routes);
app.use('/me', routes);
app.use('/who/:name?/:title?', routes);











var server = app.listen(300, function(){

	console.log("The WADE Application Port Listen At `300`");

});

/*with node
var http = require('http'); add the http module
var myServer = http.createServer(function (req, res) { create server
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.write("<h2 style='text-align:center'>SEC</h2>");
  res.write("<br/>");
  res.end("<p style='text-indent:20px;'>Unele persoane au dificultati in alegerea tinutei potrivite pentru un anumit tip de eveniment" +
  	"(interviu, dineu oficial, spectacol etc.). Sa se dezvolte o aplicatie Web care permite alegerea pieselor"  +
  	"de imbracaminte corespunzatoare pe baza garderobei existente - diversele informatii de interes pot fi " +
  	"preluate de la DBpedia sau Freebase. Se vor oferi sugestii conform tendintelor modei, sezonului, " +
  	"cromaticii dorite si/sau stilului vestimentar adoptat de persoana respectiva. De asemenea, se vor pune"  +
  	"la dispozitie informatii utile referitoare la achizitionarea unor produse de interes, in functie de " +
  	"localizarea geografica a utilizatorului.</p>\n");
});
	myServer.listen(300, '127.0.0.1');  server listen at port 300
console.log('Server running at port 300');*/