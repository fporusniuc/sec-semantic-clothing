# SEC(Semantic Clothing) #


### Description ###
```
  Unele persoane au dificultăţi în alegerea ţinutei potrivite pentru un anumit tip de eveniment (interviu, dineu oficial, spectacol etc.). Să se dezvolte o aplicaţie Web care
permite alegerea pieselor de îmbrăcăminte corespunzătoare pe baza garderobei existente – diversele informaţii de interes pot fi preluate de la DBpedia sau Freebase. Se vor oferi
sugestii conform tendinţelor modei, sezonului, cromaticii dorite şi/sau stilului vestimentar adoptat de persoana respectivă. De asemenea, se vor pune la dispoziţie informaţii utile
referitoare la achiziţionarea unor produse de interes, în funcţie de localizarea geografică a utilizatorului.
```
### How do I get set up? ###

* To run this app you must have installed NodeJs on your machine and MongoDB.
* After you did that you must open a git bash window on your folder where you have the clone of the project and run like this:
* You must run this in the git bash or cmd `node index.js`. Do not forget to be situated in the folder with the project

### Contribution guidelines ###

* No tests yet
* No code review yet

### Who do I talk to? ###

#### Team ####
* Florin Porușniuc
* Adrian Ciucănel
* Mădălina Vătămănelu


### Code sample: ###
#### package.json file: ####

```
{
  "name": "sec",
  "version": "1.0.0",
  "description": "Unele persoane au dificultăţi în alegerea ţinutei potrivite pentru un anumit tip de eveniment (interviu, dineu oficial, spectacol etc.). Să se dezvolte o aplicaţie Web care permite alegerea pieselor de îmbrăcăminte corespunzătoare pe baza garderobei existente – diversele informaţii de interes pot fi preluate de la DBpedia sau Freebase. Se vor oferi sugestii conform tendinţelor modei, sezonului, cromaticii dorite şi/sau stilului vestimentar adoptat de persoana respectivă. De asemenea, se vor pune la dispoziţie informaţii utile referitoare la achiziţionarea unor produse de interes, în funcţie de localizarea geografică a utilizatorului.",
  "main": "app.js",
  "scripts": {
    "test": "index.js"
  },
  "repository": {
    "type": "git",
    "url": "https://fporusniuc@bitbucket.org/fporusniuc/sec-semantic-clothing.git"
  },
  "keywords": [
    "clothing",
    "RDFA",
    "DBpedia",
    "Freebase",
    "Web"
  ],
  "author": "Florin, Adi, Mada",
  "license": "ISC",
  "dependencies": {
    "express": "^4.10.7"
  }
}
```
