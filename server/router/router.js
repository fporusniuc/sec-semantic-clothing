var express = require('express');
var router = express.Router();
var mdbClient = require('mongodb').MongoClient;
var stardog = require("stardog");
var stardogFunctions = require("../modules/stardog-functions.js");
var wardrobeFunctions = require("../modules/wardrobe/wardrobe.js");
var categoryFunctions = require("../modules/wardrobe/category.js");
var colourFunctions = require("../modules/wardrobe/colours.js");
var eventFunctions = require("../modules/wardrobe/events.js");
var seasonsFunctions = require("../modules/wardrobe/seasons.js");
var stylesFunctions = require("../modules/wardrobe/styles.js");
var userFunctions = require("../modules/user.js");


/*__________________________________  INDEX -routes ________________________________________________*/
/* GET home page. */
router.get('/', function(req,res){
  
  	res.render('./pages/login', 
      { name:'Aceasta este o variabila trimisa spre ejs', 
        users:['Florin','Adi','Mada']
      });
    console.log("This is Index");

/* userFunctions.loginUser("memada","password",function(result) {
     console.log(result);
  });*/
 /* var props = {"username":"leonard","firstName":"Leonard","lastName":"Astro","country":"Romania","gender":"Male",
    "password":"password"};
  userFunctions.registerUser(props,function(result) {
     console.log(result);
  });*/
 eventFunctions.findItemsForEvent("leonard","SportEvent",function(result) {
     console.log(result);
  });
  /*eventFunctions.findItemsForEventAndStyleAndSeason("memada","SportEvent","Business","Winter",function(result) {
     console.log(result);
  });*/
/*eventFunctions.findShoesForEventAndStyleAndSeasonAndColour("memada","SportEvent","Sport","Winter","Brown",function(result) {
     console.log(result);
  });*/
  /*eventFunctions.findItemsForEventAndSeason("memada","SportEvent","Winter",function(result) {
     console.log(result);
  });*/
  /*  wardrobeFunctions.findAllItemsForUser("memada",function(result) {
     console.log(result);
  });
     wardrobeFunctions.findAllClothingForUser("memada",function(result) {
     console.log(result);
  });
      wardrobeFunctions.findAllShoesForUser("memada",function(result) {
     console.log(result);
  });*/
  /*   wardrobeFunctions.findClothingWithCategoryForUser("memada","Dress",function(result) {
     console.log(result);
  });
      wardrobeFunctions.findShoesWithCategoryForUser("memada","Oxford",function(result) {
     console.log(result);
  });*/

/*var props = {"username":"memada","size":"30", "description":"descriereee","url":"http://mada.org",
"category":"Tshirt","name":"BlackTshirt3","colour":"Black"};
wardrobeFunctions.addItemInWardrobe(props,function(result){
  console.log(result);
});*/

   //Use example
  /*categoryFunctions.getAllCategories(function(result) {
     console.log(result);
  });
   categoryFunctions.getClothingCategories(function(result) {
     console.log(result);
  });
    categoryFunctions.getShoesCategories(function(result) {
     console.log(result);
  });*/

  /*
   categoryFunctions.getAllCategoriesInstances( function(result) {
     console.log(result);
  });
  
   categoryFunctions.getClothingCategoriesInstances( function(result) {
     console.log(result);
  });
    categoryFunctions.getShoesCategoriesInstances( function(result) {
     console.log(result);
  });*/

/*colourFunctions.getAllColours(function(result) {
     console.log(result);
  });

eventFunctions.getAllEvents( function(result) {
     console.log(result);
  });

seasonsFunctions.getAllSeasons( function(result) {
     console.log(result);
  });

stylesFunctions.getAllStyles( function(result) {
     console.log(result);
  });*/

    
  // stardogFunctions.insertTriplesInDB(triple);   
   /*stardogFunctions.sizeDB(); */ 

});
/*`````````````````````````````````````````````````````````````````````````````````````````*/
/*__________________________________  INDEX -routes ________________________________________________*/
/* GET home page. */
router.get('/reset', function(req,res){
    res.render('./pages/reset', 
      { name:'Aceasta este o variabila trimisa spre ejs', 
        users:['Florin','Adi','Mada']
      });
    console.log("This is Index");
});
/*``
/*__________________________________  signup -routes ________________________________________________*/
/* GET signup page. */
router.get('/signup', function(req,res){
    res.render('./pages/account', 
      { name:'Aceasta este o variabila trimisa spre ejs', 
        users:['Florin','Adi','Mada']
      });
    console.log("This is signup");
});
/*`````````````````````````````````````````````````````````````````````````````````````````*/

/*__________________________________  signup -routes ________________________________________________*/
/* GET signup page. */
router.get('/home', function(req,res){
    res.render('./pages/home', 
      { name:'Aceasta este o variabila trimisa spre ejs', 
        users:['Florin','Adi','Mada']
      });
    console.log("This is signup");
});
/*`````````````````````````````````````````````````````````````````````````````````````````*/



/*-------------------------------------------------------*/
router.get('/login', function(req,res){
   res.render('./pages/login', 
      { name:'Aceasta este o variabila trimisa spre ejs', 
        users:['Florin','Adi','Mada']
      });
    console.log("login");
});


/*-------------------------------------------------------*/
router.get('/account', function(req,res){
  res.render('./pages/account', {
        title : 'Control Panel',
        countries : [
    {short:"  " , name:"Please select a country"},
    {short:"AF" , name:"Afghanistan"},
    {short:"AL" , name:"Albania"},
    {short:"DZ" , name:"Algeria"},
    {short:"AS" , name:"American Samoa"},
    {short:"AD" , name:"Andorra"},
    {short:"AO" , name:"Angola"},
    {short:"AI" , name:"Anguilla"},
    {short:"AQ" , name:"Antarctica"},
    {short:"AG" , name:"Antigua and Barbuda"},
    {short:"AR" , name:"Argentina"},
    {short:"AM" , name:"Armenia"},
    {short:"AW" , name:"Aruba"},
    {short:"AU" , name:"Australia"},
    {short:"AT" , name:"Austria"},
    {short:"AZ" , name:"Azerbaijan"},
    {short:"BS" , name:"Bahamas"},
    {short:"BH" , name:"Bahrain"},
    {short:"BD" , name:"Bangladesh"},
    {short:"BB" , name:"Barbados"},
    {short:"BY" , name:"Belarus"},
    {short:"BE" , name:"Belgium"},
    {short:"BZ" , name:"Belize"},
    {short:"BJ" , name:"Benin"},
    {short:"BM" , name:"Bermuda"},
    {short:"BT" , name:"Bhutan"},
    {short:"BO" , name:"Bolivia"},
    {short:"BA" , name:"Bosnia and Herzegowina"},
    {short:"BW" , name:"Botswana"},
    {short:"BV" , name:"Bouvet Island"},
    {short:"BR" , name:"Brazil"},
    {short:"IO" , name:"British Indian Ocean Territory"},
    {short:"BN" , name:"Brunei Darussalam"},
    {short:"BG" , name:"Bulgaria"},
    {short:"BF" , name:"Burkina Faso"},
    {short:"BI" , name:"Burundi"},
    {short:"KH" , name:"Cambodia"},
    {short:"CM" , name:"Cameroon"},
    {short:"CA" , name:"Canada"},
    {short:"CV" , name:"Cape Verde"},
    {short:"KY" , name:"Cayman Islands"},
    {short:"CF" , name:"Central African Republic"},
    {short:"TD" , name:"Chad"},
    {short:"CL" , name:"Chile"},
    {short:"CN" , name:"China"},
    {short:"CX" , name:"Christmas Island"},
    {short:"CC" , name:"Cocos (Keeling) Islands"},
    {short:"CO" , name:"Colombia"},
    {short:"KM" , name:"Comoros"},
    {short:"CG" , name:"Congo"},
    {short:"CD" , name:"Congo, the Democratic Republic of the"},
    {short:"CK" , name:"Cook Islands"},
    {short:"CR" , name:"Costa Rica"},
    {short:"CI" , name:"Cote d'Ivoire"},
    {short:"HR" , name:"Croatia (Hrvatska)"},
    {short:"CU" , name:"Cuba"},
    {short:"CY" , name:"Cyprus"},
    {short:"CZ" , name:"Czech Republic"},
    {short:"DK" , name:"Denmark"},
    {short:"DJ" , name:"Djibouti"},
    {short:"DM" , name:"Dominica"},
    {short:"DO" , name:"Dominican Republic"},
    {short:"TP" , name:"East Timor"},
    {short:"EC" , name:"Ecuador"},
    {short:"EG" , name:"Egypt"},
    {short:"SV" , name:"El Salvador"},
    {short:"GQ" , name:"Equatorial Guinea"},
    {short:"ER" , name:"Eritrea"},
    {short:"EE" , name:"Estonia"},
    {short:"ET" , name:"Ethiopia"},
    {short:"FK" , name:"Falkland Islands (Malvinas)"},
    {short:"FO" , name:"Faroe Islands"},
    {short:"FJ" , name:"Fiji"},
    {short:"FI" , name:"Finland"},
    {short:"FR" , name:"France"},
    {short:"FX" , name:"France, Metropolitan"},
    {short:"GF" , name:"French Guiana"},
    {short:"PF" , name:"French Polynesia"},
    {short:"TF" , name:"French Southern Territories"},
    {short:"GA" , name:"Gabon"},
    {short:"GM" , name:"Gambia"},
    {short:"GE" , name:"Georgia"},
    {short:"DE" , name:"Germany"},
    {short:"GH" , name:"Ghana"},
    {short:"GI" , name:"Gibraltar"},
    {short:"GR" , name:"Greece"},
    {short:"GL" , name:"Greenland"},
    {short:"GD" , name:"Grenada"},
    {short:"GP" , name:"Guadeloupe"},
    {short:"GU" , name:"Guam"},
    {short:"GT" , name:"Guatemala"},
    {short:"GN" , name:"Guinea"},
    {short:"GW" , name:"Guinea-Bissau"},
    {short:"GY" , name:"Guyana"},
    {short:"HT" , name:"Haiti"},
    {short:"HM" , name:"Heard and Mc Donald Islands"},
    {short:"VA" , name:"Holy See (Vatican City State)"},
    {short:"HN" , name:"Honduras"},
    {short:"HK" , name:"Hong Kong"},
    {short:"HU" , name:"Hungary"},
    {short:"IS" , name:"Iceland"},
    {short:"IN" , name:"India"},
    {short:"ID" , name:"Indonesia"},
    {short:"IR" , name:"Iran (Islamic Republic of)"},
    {short:"IQ" , name:"Iraq"},
    {short:"IE" , name:"Ireland"},
    {short:"IL" , name:"Israel"},
    {short:"IT" , name:"Italy"},
    {short:"JM" , name:"Jamaica"},
    {short:"JP" , name:"Japan"},
    {short:"JO" , name:"Jordan"},
    {short:"KZ" , name:"Kazakhstan"},
    {short:"KE" , name:"Kenya"},
    {short:"KI" , name:"Kiribati"},
    {short:"KP" , name:"Korea, Democratic People's Republic of"},
    {short:"KR" , name:"Korea, Republic of"},
    {short:"KW" , name:"Kuwait"},
    {short:"KG" , name:"Kyrgyzstan"},
    {short:"LA" , name:"Lao People's Democratic Republic"},
    {short:"LV" , name:"Latvia"},
    {short:"LB" , name:"Lebanon"},
    {short:"LS" , name:"Lesotho"},
    {short:"LR" , name:"Liberia"},
    {short:"LY" , name:"Libyan Arab Jamahiriya"},
    {short:"LI" , name:"Liechtenstein"},
    {short:"LT" , name:"Lithuania"},
    {short:"LU" , name:"Luxembourg"},
    {short:"MO" , name:"Macau"},
    {short:"MK" , name:"Macedonia, The Former Yugoslav Republic of"},
    {short:"MG" , name:"Madagascar"},
    {short:"MW" , name:"Malawi"},
    {short:"MY" , name:"Malaysia"},
    {short:"MV" , name:"Maldives"},
    {short:"ML" , name:"Mali"},
    {short:"MT" , name:"Malta"},
    {short:"MH" , name:"Marshall Islands"},
    {short:"MQ" , name:"Martinique"},
    {short:"MR" , name:"Mauritania"},
    {short:"MU" , name:"Mauritius"},
    {short:"YT" , name:"Mayotte"},
    {short:"MX" , name:"Mexico"},
    {short:"FM" , name:"Micronesia, Federated States of"},
    {short:"MD" , name:"Moldova, Republic of"},
    {short:"MC" , name:"Monaco"},
    {short:"MN" , name:"Mongolia"},
    {short:"MS" , name:"Montserrat"},
    {short:"MA" , name:"Morocco"},
    {short:"MZ" , name:"Mozambique"},
    {short:"MM" , name:"Myanmar"},
    {short:"NA" , name:"Namibia"},
    {short:"NR" , name:"Nauru"},
    {short:"NP" , name:"Nepal"},
    {short:"NL" , name:"Netherlands"},
    {short:"AN" , name:"Netherlands Antilles"},
    {short:"NC" , name:"New Caledonia"},
    {short:"NZ" , name:"New Zealand"},
    {short:"NI" , name:"Nicaragua"},
    {short:"NE" , name:"Niger"},
    {short:"NG" , name:"Nigeria"},
    {short:"NU" , name:"Niue"},
    {short:"NF" , name:"Norfolk Island"},
    {short:"MP" , name:"Northern Mariana Islands"},
    {short:"NO" , name:"Norway"},
    {short:"OM" , name:"Oman"},
    {short:"PK" , name:"Pakistan"},
    {short:"PW" , name:"Palau"},
    {short:"PA" , name:"Panama"},
    {short:"PG" , name:"Papua New Guinea"},
    {short:"PY" , name:"Paraguay"},
    {short:"PE" , name:"Peru"},
    {short:"PH" , name:"Philippines"},
    {short:"PN" , name:"Pitcairn"},
    {short:"PL" , name:"Poland"},
    {short:"PT" , name:"Portugal"},
    {short:"PR" , name:"Puerto Rico"},
    {short:"QA" , name:"Qatar"},
    {short:"RE" , name:"Reunion"},
    {short:"RO" , name:"Romania"},
    {short:"RU" , name:"Russian Federation"},
    {short:"RW" , name:"Rwanda"},
    {short:"KN" , name:"Saint Kitts and Nevis"}, 
    {short:"LC" , name:"Saint LUCIA"},
    {short:"VC" , name:"Saint Vincent and the Grenadines"},
    {short:"WS" , name:"Samoa"},
    {short:"SM" , name:"San Marino"},
    {short:"ST" , name:"Sao Tome and Principe"}, 
    {short:"SA" , name:"Saudi Arabia"},
    {short:"SN" , name:"Senegal"},
    {short:"SC" , name:"Seychelles"},
    {short:"SL" , name:"Sierra Leone"},
    {short:"SG" , name:"Singapore"},
    {short:"SK" , name:"Slovakia (Slovak Republic)"},
    {short:"SI" , name:"Slovenia"},
    {short:"SB" , name:"Solomon Islands"},
    {short:"SO" , name:"Somalia"},
    {short:"ZA" , name:"South Africa"},
    {short:"GS" , name:"South Georgia and the South Sandwich Islands"},
    {short:"SS" , name:"South Sudan"},
    {short:"ES" , name:"Spain"},
    {short:"LK" , name:"Sri Lanka"},
    {short:"SH" , name:"St. Helena"},
    {short:"PM" , name:"St. Pierre and Miquelon"},
    {short:"SD" , name:"Sudan"},
    {short:"SR" , name:"Suriname"},
    {short:"SJ" , name:"Svalbard and Jan Mayen Islands"},
    {short:"SZ" , name:"Swaziland"},
    {short:"SE" , name:"Sweden"},
    {short:"CH" , name:"Switzerland"},
    {short:"SY" , name:"Syrian Arab Republic"},
    {short:"TW" , name:"Taiwan, Province of China"},
    {short:"TJ" , name:"Tajikistan"},
    {short:"TZ" , name:"Tanzania, United Republic of"},
    {short:"TH" , name:"Thailand"},
    {short:"TG" , name:"Togo"},
    {short:"TK" , name:"Tokelau"},
    {short:"TO" , name:"Tonga"},
    {short:"TT" , name:"Trinidad and Tobago"},
    {short:"TN" , name:"Tunisia"},
    {short:"TR" , name:"Turkey"},
    {short:"TM" , name:"Turkmenistan"},
    {short:"TC" , name:"Turks and Caicos Islands"},
    {short:"TV" , name:"Tuvalu"},
    {short:"UG" , name:"Uganda"},
    {short:"UA" , name:"Ukraine"},
    {short:"AE" , name:"United Arab Emirates"},
    {short:"GB" , name:"United Kingdom"},
    {short:"US" , name:"United States"},
    {short:"UM" , name:"United States Minor Outlying Islands"},
    {short:"UY" , name:"Uruguay"},
    {short:"UZ" , name:"Uzbekistan"},
    {short:"VU" , name:"Vanuatu"},
    {short:"VE" , name:"Venezuela"},
    {short:"VN" , name:"Viet Nam"},
    {short:"VG" , name:"Virgin Islands (British)"},
    {short:"VI" , name:"Virgin Islands (U.S.)"},
    {short:"WF" , name:"Wallis and Futuna Islands"},
    {short:"EH" , name:"Western Sahara"},
    {short:"YE" , name:"Yemen"},
    {short:"YU" , name:"Yugoslavia"},
    {short:"ZM" , name:"Zambia"},
    {short:"ZW" , name:"Zimbabwe"}
],
        udata : req.session.user
      });
    console.log("This is account");
});


/*-------------------------------------------------------*/
router.get('/who/:name?/:title?', function(req,res){
  var name = req.params.name;
  var title = req.params.title;
  res.send(name + " " + "is before" + " " + title + " " +"in this situation." );
    console.log(name + " " + "is before" + " " + title + " " +"in this situation.");
});

/*-------------------------------------------------------*/
// router.get('/*', function(req,res){
//   res.send("<h1>404 Not Found this Route</h1>");
//     console.log("404 Not Found this Route");
// });

/*


var CT = require('./modules/country-list');
var AM = require('./modules/account-manager');
var EM = require('./modules/email-dispatcher');

module.exports = function(app) {

// main login page //

  app.get('/', function(req, res){
  // check if the user's credentials are saved in a cookie //
    if (req.cookies.user == undefined || req.cookies.pass == undefined){
      res.render('login', { title: 'Hello - Please Login To Your Account' });
    } else{
  // attempt automatic login //
      AM.autoLogin(req.cookies.user, req.cookies.pass, function(o){
        if (o != null){
            req.session.user = o;
          res.redirect('/home');
        } else{
          res.render('login', { title: 'Hello - Please Login To Your Account' });
        }
      });
    }
  });
  
  app.post('/', function(req, res){
    AM.manualLogin(req.param('user'), req.param('pass'), function(e, o){
      if (!o){
        res.send(e, 400);
      } else{
          req.session.user = o;
        if (req.param('remember-me') == 'true'){
          res.cookie('user', o.user, { maxAge: 900000 });
          res.cookie('pass', o.pass, { maxAge: 900000 });
        }
        res.send(o, 200);
      }
    });
  });
  
// logged-in user homepage //
  
  app.get('/home', function(req, res) {
      if (req.session.user == null){
  // if user is not logged-in redirect back to login page //
          res.redirect('/');
      }   else{
      res.render('home', {
        title : 'Control Panel',
        countries : CT,
        udata : req.session.user
      });
      }
  });
  
  app.post('/home', function(req, res){
    if (req.param('user') != undefined) {
      AM.updateAccount({
        user    : req.param('user'),
        name    : req.param('name'),
        email     : req.param('email'),
        country   : req.param('country'),
        pass    : req.param('pass')
      }, function(e, o){
        if (e){
          res.send('error-updating-account', 400);
        } else{
          req.session.user = o;
      // update the user's login cookies if they exists //
          if (req.cookies.user != undefined && req.cookies.pass != undefined){
            res.cookie('user', o.user, { maxAge: 900000 });
            res.cookie('pass', o.pass, { maxAge: 900000 }); 
          }
          res.send('ok', 200);
        }
      });
    } else if (req.param('logout') == 'true'){
      res.clearCookie('user');
      res.clearCookie('pass');
      req.session.destroy(function(e){ res.send('ok', 200); });
    }
  });
  
// creating new accounts //
  
  app.get('/signup', function(req, res) {
    res.render('signup', {  title: 'Signup', countries : CT });
  });
  
  app.post('/signup', function(req, res){
    AM.addNewAccount({
      name  : req.param('name'),
      email   : req.param('email'),
      user  : req.param('user'),
      pass  : req.param('pass'),
      country : req.param('country')
    }, function(e){
      if (e){
        res.send(e, 400);
      } else{
        res.send('ok', 200);
      }
    });
  });

// password reset //

  app.post('/lost-password', function(req, res){
  // look up the user's account via their email //
    AM.getAccountByEmail(req.param('email'), function(o){
      if (o){
        res.send('ok', 200);
        EM.dispatchResetPasswordLink(o, function(e, m){
        // this callback takes a moment to return //
        // should add an ajax loader to give user feedback //
          if (!e) {
          //  res.send('ok', 200);
          } else{
            res.send('email-server-error', 400);
            for (k in e) console.log('error : ', k, e[k]);
          }
        });
      } else{
        res.send('email-not-found', 400);
      }
    });
  });

  app.get('/reset-password', function(req, res) {
    var email = req.query["e"];
    var passH = req.query["p"];
    AM.validateResetLink(email, passH, function(e){
      if (e != 'ok'){
        res.redirect('/');
      } else{
  // save the user's email in a session instead of sending to the client //
        req.session.reset = { email:email, passHash:passH };
        res.render('reset', { title : 'Reset Password' });
      }
    })
  });
  
  app.post('/reset-password', function(req, res) {
    var nPass = req.param('pass');
  // retrieve the user's email from the session to lookup their account and reset password //
    var email = req.session.reset.email;
  // destory the session immediately after retrieving the stored email //
    req.session.destroy();
    AM.updatePassword(email, nPass, function(e, o){
      if (o){
        res.send('ok', 200);
      } else{
        res.send('unable to update password', 400);
      }
    })
  });
  
// view & delete accounts //
  
  app.get('/print', function(req, res) {
    AM.getAllRecords( function(e, accounts){
      res.render('print', { title : 'Account List', accts : accounts });
    })
  });
  
  app.post('/delete', function(req, res){
    AM.deleteAccount(req.body.id, function(e, obj){
      if (!e){
        res.clearCookie('user');
        res.clearCookie('pass');
              req.session.destroy(function(e){ res.send('ok', 200); });
      } else{
        res.send('record not found', 400);
      }
      });
  });
  
  app.get('/reset', function(req, res) {
    AM.delAllRecords(function(){
      res.redirect('/print'); 
    });
  });
  
  app.get('*', function(req, res) { res.render('404', { title: 'Page Not Found'}); });

};

*/

module.exports = router;