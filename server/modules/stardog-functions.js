
var stardog = require("stardog");

var endpoint = "http://localhost:5820/";
var user = "admin";
var password = "admin";
var databaseName = "secDB";

var stardogConnection = new stardog.Connection();   
stardogConnection.setEndpoint(endpoint);
stardogConnection.setCredentials(user, password);

//prefix sintax for insert
var prefixsInsert = "@prefix sec: <http://students.info.uaic.ro/~madalina.vatamanelu/wade/sec.owl#> .\n"+
              "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n"+
              "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"+
              "@prefix owl: <http://www.w3.org/2002/07/owl#> .\n";

//prefix sintax for query
var prefixsQuery = "prefix sec: <http://students.info.uaic.ro/~madalina.vatamanelu/wade/sec.owl#>\n"+
              "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"+
              "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"+
              "prefix owl: <http://www.w3.org/2002/07/owl#>\n";


exports.queryDB = function(queryString,callback){
	 stardogConnection.query({ 
        database: databaseName, 
        query: queryString,  
        offset: 0 
    },
    function (data) {
        callback(data.results.bindings);
    });
}

exports.queryAskDB = function(queryString,callback){
     stardogConnection.query({ 
        database: databaseName, 
        query: queryString,  
        offset: 0 
    },
    function (data) {
        callback(data);
    });
}

exports.insertTriplesInDB = function(triples,callback){
    stardogConnection.onlineDB( { database: databaseName, strategy: "NO_WAIT" }, function () {
                stardogConnection.begin({ database: databaseName }, function (body, response) {
                    txId = body;
                    stardogConnection.addInTransaction(
                        { database: databaseName, "txId": txId, "body": prefixsInsert+triples, contentType: "text/turtle" },
                        function (body2, response2) {
                            stardogConnection.commit({ database: databaseName, "txId": txId }, function (body3, response3) {
                                // query for the triple added.
                                stardogConnection.query(
                                    { 
                                        database: databaseName,
                                        "query": prefixsQuery + " ask where { "+  triples +"}",
                                        mimetype: "text/boolean"
                                    },
                                    function (dataQ, responseQ) {
                                        console.log("Triples : "+ triples +" inserted : " + dataQ);
                                        callback(dataQ);
                                        if(dataQ!=true){
                                            stardogConnection.rollback({ database: databaseName, "txId": txId }, function (body3, response3) {
                                            });

                                        }
                                    }
                                );
                            });
                        }
                    );
                });
            });
}

exports.sizeDB =function(){
	stardogConnection.onlineDB({ database: databaseName }, function () {
                stardogConnection.getDBSize({ database: databaseName }, function (response) {
                    var sizeNum = parseInt(response);
                    console.log("Database "+databaseName+" size : "+sizeNum);
                });       
            });
}