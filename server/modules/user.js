var stardogFunctions = require("./stardog-functions.js");

var prefixs = 'PREFIX sec:<http://students.info.uaic.ro/~madalina.vatamanelu/wade/sec.owl#>'+
			  'PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>'+
			  'PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>'+
			  'PREFIX owl:<http://www.w3.org/2002/07/owl#>';

//properties must have: username,first name, last name, passsword, country, gender
exports.registerUser = function( properties, callback){
	var response = {};
	var username = properties["username"];

	existUser(username,function(result){
		if(result == true){
			response["response"] = "User already in database";
			return callback(response);
		}
		else{		
			var usernameNS = "sec:" + properties["username"] + "User";
			var password =  properties["password"];
			var firstName = properties["firstName"] ;
			var lastName = properties["lastName"];
			var country = properties["country"];
			var gender = properties["gender"];
			var wardrobe = "sec:"+username + "Wardrobe";

			var instanceTriple = usernameNS + " rdf:type " + "owl:NamedIndividual . ";
			var typeTriple = usernameNS + " rdf:type " + "sec:User . ";
			var passwordTriple = usernameNS + " sec:password " +  "\"" + password +  "\"" +". ";
			var lastNameTriple = usernameNS + " sec:lastName " +  "\"" + firstName +  "\"" + " . ";
			var firstNameTriple = usernameNS + " sec:firstName " +  "\"" + lastName +  "\"" + " . ";
			var usernameTriple = usernameNS + " sec:username " +  "\"" + username  +  "\"" +" . ";
			var countryTriple = usernameNS + " sec:country " +  "\"" + country +  "\"" +" . ";
			var genderTriple = usernameNS + " sec:gender " +  "\"" + gender +  "\"" +" . ";
			var wardrobeTriple = wardrobe + " rdf:type " + "owl:NamedIndividual . ";
			var wardrobeTriple2 = wardrobe + " rdf:type " + "sec:Wardrobe . ";
			var wardrobeTriple3 = usernameNS + " sec:hasWardrobe " + wardrobe + " . ";
			var wardrobeTriple4 = wardrobe + " sec:hasUser " + usernameNS + " . ";

			var userTriples = instanceTriple + typeTriple + passwordTriple + lastNameTriple + firstNameTriple 
								+ usernameTriple + countryTriple + genderTriple 
								+ wardrobeTriple + wardrobeTriple2 + wardrobeTriple3 +wardrobeTriple4;
		    stardogFunctions.insertTriplesInDB( userTriples,function(result){
		    	if(result == true){
		    		response["response"] = "User successfully added";
		    		return callback(response);
		    	}
		    	else{
		    		response["response"] = "User wasn't add";
		    		return callback(response);
		    	}
		    	
		    });
		}	
	});
	
}

exports.loginUser = function(username,password,callback){
	var query = prefixs + "ask where {?user rdf:type sec:User. ?user sec:username " + "\""+ username +"\" ."
				+ "?user sec:password " + "\""+ password +"\" .}";
	var ask = false;
	stardogFunctions.queryAskDB(query,function(result){
		if(String(result).indexOf("true")>-1)
			ask = true;		
	    callback(ask);
	});
}

existUser = function(username,callback){
	var query = prefixs + "ask where {?user rdf:type sec:User. ?user sec:username " + "\""+ username +"\"}";

	var ask = false;
	stardogFunctions.queryAskDB(query,function(result){
		if(String(result).indexOf("true")>-1)
			ask = true;		
	    callback(ask);
	});
}
