var stardogFunctions = require("./stardog-functions.js");

var prefixs = 'PREFIX sec:<http://students.info.uaic.ro/~madalina.vatamanelu/wade/sec.owl#>'+
			  'PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>'+
			  'PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>'+
			  'PREFIX owl:<http://www.w3.org/2002/07/owl#>';

var queryAllCategories = 	prefixs +
							'SELECT * WHERE {'+
 							'{?class (rdfs:subClassOf)* sec:Clothing  FILTER(?class!=sec:Clothing)}'+
 							'UNION'+
 							'{?class (rdfs:subClassOf)* sec:Shoes FILTER(?class!=sec:Shoes)}}';

var queryClothingCategories = 	prefixs +
							'SELECT * WHERE {'+
 							'{?class (rdfs:subClassOf)* sec:Clothing  FILTER(?class!=sec:Clothing)}}';

var queryShoesCategories = 	'PREFIX sec:<http://students.info.uaic.ro/~madalina.vatamanelu/wade/sec.owl#>'+
							'PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>'+
							'SELECT * WHERE {'+
 							'{?class (rdfs:subClassOf)* sec:Shoes  FILTER(?class!=sec:Shoes)}}';


var queryAllCategoriesInstances = 	prefixs +
							'SELECT * WHERE {'+
 							'{?class (rdfs:subClassOf)* sec:Clothing. ?instance rdf:type ?class  FILTER(?class!=sec:Clothing)}'+
 							'UNION'+
 							'{?class (rdfs:subClassOf)* sec:Shoes. ?instance rdf:type ?class   FILTER(?class!=sec:Shoes)}}';

var queryClothingCategoriesInstances = 	prefixs +
							'SELECT * WHERE {'+
 							'{?class (rdfs:subClassOf)* sec:Clothing. ?clothing rdf:type ?class  FILTER(?class!=sec:Clothing)}}';

var queryShoesCategoriesInstances = 	prefixs +
							'SELECT * WHERE {'+
 							'{?class (rdfs:subClassOf)* sec:Shoes. ?shoes rdf:type ?class  FILTER(?class!=sec:Shoes)}}';


exports.getAllCategories = function(callback){
	  var jsonData = {};
    var categoriesArray = [];

    stardogFunctions.queryDB(queryAllCategories, function (result) {
        for(var i=0;i<result.length;i++){
       	 var lastPart = result[i].class.value.split("#").pop();
       	 categoriesArray.push(lastPart);
		}
		jsonData["categories"]=categoriesArray;
    	callback(jsonData);
    });
}

exports.getClothingCategories = function(callback){
	  var jsonData = {};
    var clothingCategoriesArray = [];

   stardogFunctions.queryDB(queryClothingCategories, function (result) {
        for(var i=0;i<result.length;i++){
       	 var lastPart = result[i].class.value.split("#").pop();
       	 clothingCategoriesArray.push(lastPart);
		}
		jsonData["clothing-categories"]=clothingCategoriesArray;
    	callback(jsonData);
    });
}

exports.getShoesCategories = function(callback){
	  var jsonData = {};
    var shoesCategoriesArray = [];

    stardogFunctions.queryDB( queryShoesCategories,function (result) {
        for(var i=0;i<result.length;i++){
       	 var lastPart = result[i].class.value.split("#").pop();
       	 shoesCategoriesArray.push(lastPart);
		}
		jsonData["shoes-categories"]= shoesCategoriesArray;
    	callback(jsonData);
    });
}

exports.getAllCategoriesInstances = function( callback){
	  var jsonData = {};
    var categoriesInstancesArray = [];

    stardogFunctions.queryDB( queryAllCategoriesInstances,  function (result) {
        for(var i=0;i<result.length;i++){
       	 var lastPart = result[i].instance.value.split("#").pop();
       	 categoriesInstancesArray.push(lastPart);
		}
		jsonData["categories-instances"]=categoriesInstancesArray;
    	callback(jsonData);
    });
}

exports.getClothingCategoriesInstances = function( callback){
	  var jsonData = {};
    var clothingCategoriesInstancesArray = [];

    stardogFunctions.queryDB( queryClothingCategoriesInstances, function (result) {
        for(var i=0;i<result.length;i++){
       	 var lastPart = result[i].clothing.value.split("#").pop();
       	 clothingCategoriesInstancesArray.push(lastPart);
		}
		jsonData["clothing-instance"]=clothingCategoriesInstancesArray;
    	callback(jsonData);
    });
}

exports.getShoesCategoriesInstances = function(callback){
	  var jsonData = {};
    var shoesCategoriesInstancesArray = [];

    stardogFunctions.queryDB( queryShoesCategoriesInstances, function (result) {
        for(var i=0;i<result.length;i++){
       	 var lastPart = result[i].shoes.value.split("#").pop();
       	 shoesCategoriesInstancesArray.push(lastPart);
		}
		jsonData["shoes-instances"]=shoesCategoriesInstancesArray;
    	callback(jsonData);
    });
}