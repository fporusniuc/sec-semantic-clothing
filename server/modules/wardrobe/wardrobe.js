var stardogFunctions = require("../stardog-functions.js");

var prefixs = 'PREFIX sec:<http://students.info.uaic.ro/~madalina.vatamanelu/wade/sec.owl#>'+
			  'PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>'+
			  'PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>'+
			  'PREFIX owl:<http://www.w3.org/2002/07/owl#>';
				''

//properties must have: item name,username, category, colour, size, description
exports.addItemInWardrobe = function( properties, callback){
	var response = {};
	var name = "sec:" + properties["name"];
	var category = "sec:" + properties["category"];
	var colour = "sec:" + properties["colour"] + "Colour";
	var description = properties["description"];
	var size = properties["size"];
	var url = properties["url"];
	var username = properties["username"];

	var instanceTriple = name + " rdf:type " + "owl:NamedIndividual .";
	var categoryTriple = name + " rdf:type " + category +" .";
	var sizeTriple = name + " sec:size " + size +" .";
	var urlTriple = name + " sec:url " +  "\"" + url + "\""+ " .";
	var descriptionTriple = name + " sec:description " + "\"" + description + "\"" + " .";
	var colourTriple = name + " sec:colour " + colour +" .";
	var wardrobeTriple = name + " sec:hasWardrobe " + "sec:"+username+"Wardrobe . ";
	var wardrobeTriple2 = "sec:"+username+"Wardrobe " + " sec:contains " + name +" .";

	var wardrobeTriples = instanceTriple + categoryTriple + colourTriple + sizeTriple + urlTriple
	                    + descriptionTriple+wardrobeTriple+wardrobeTriple2;
    stardogFunctions.insertTriplesInDB( wardrobeTriples, function(result){
		    	if(result == true){
		    		response["response"] = "Item successfully added";
		    		return callback(response);
		    	}
		    	else{
		    		response["response"] = "Item wasn't add";
		    		return callback(response);
		    	}
		    	
		    });
}

// /wardrobe
exports.findAllItemsForUser = function(username,callback){
	var queryAllItems = prefixs + 'select ?item ?category ?colour ?url ?size ?description {{ ?user rdf:type sec:User. ?user sec:username \"' + username + '\" .' +
  								  '?wardrobe sec:hasUser ?user. ?class rdfs:subClassOf sec:Clothing.'+
  								  '?item rdf:type ?class. ?wardrobe sec:contains ?item. ?item sec:url ?url. '+
  								  '?item sec:description ?description. ?item sec:size ?size. ?item rdf:type ?category. '+
  								  '?item sec:colour ?c. ?c rdf:type ?colour ' +
                                  'FILTER(?category != owl:NamedIndividual && ?colour != owl:NamedIndividual) }' +
                                  'UNION' +
                                  '{ ?user rdf:type sec:User. ?user sec:username \"' + username + '\" .' +
  								  '?wardrobe sec:hasUser ?user. ?class rdfs:subClassOf sec:Shoes.'+
  								  '?item rdf:type ?class. ?wardrobe sec:contains ?item. ?item sec:url ?url. '+
  								  '?item sec:description ?description. ?item sec:size ?size. ?item rdf:type ?category. '+
  								  '?item sec:colour ?c. ?c rdf:type ?colour ' +
                                  'FILTER(?category != owl:NamedIndividual && ?colour != owl:NamedIndividual) }}';
 
    var jsonData = {};

    stardogFunctions.queryDB( queryAllItems,function (result) {
		jsonData["user-items"] = processResponse(result);
    	callback(jsonData);
    });
}

// /wardrobe/clothing
exports.findAllClothingForUser = function(username,callback){
	var queryAllClothing = prefixs + 'select ?item ?category ?colour ?url ?size ?description { ?user rdf:type sec:User. ?user sec:username \"' + username + '\" .' +
  								  '?wardrobe sec:hasUser ?user. ?class rdfs:subClassOf sec:Clothing.'+
  								  '?item rdf:type ?class. ?wardrobe sec:contains ?item. ?item sec:url ?url. '+
  								  '?item sec:description ?description. ?item sec:size ?size. ?item rdf:type ?category. '+
  								  '?item sec:colour ?c. ?c rdf:type ?colour ' +
                                  'FILTER(?category != owl:NamedIndividual && ?colour != owl:NamedIndividual)}';
 
    var jsonData = {};

    stardogFunctions.queryDB( queryAllClothing,function (result) {
    	jsonData["user-clothing"] = processResponse(result);
    	callback(jsonData);
	
    });
}

// /wardrobe/shoes
exports.findAllShoesForUser = function(username,callback){
	var queryAllShoes = prefixs + 'select ?item ?category ?colour ?url ?size ?description { ?user rdf:type sec:User. ?user sec:username \"' + username + '\" .' +
  								  '?wardrobe sec:hasUser ?user. ?class rdfs:subClassOf sec:Shoes.'+
  								  '?item rdf:type ?class. ?wardrobe sec:contains ?item. ?item sec:url ?url. '+
  								  '?item sec:description ?description. ?item sec:size ?size. ?item rdf:type ?category. '+
  								  '?item sec:colour ?c. ?c rdf:type ?colour ' +
                                  'FILTER(?category != owl:NamedIndividual && ?colour != owl:NamedIndividual)}';

    var jsonData = {};

    stardogFunctions.queryDB( queryAllShoes,function (result) {
		jsonData["user-shoes"] = processResponse(result);
    	callback(jsonData);
    });
}

// /wardrobe/clothing/clothing_type
exports.findClothingWithCategoryForUser = function(username,category,callback){
	var queryClothingWithType = prefixs + 'select ?item ?category ?colour ?url ?size ?description { ?user rdf:type sec:User. ?user sec:username \"' + username + '\" .' +
  								  '?wardrobe sec:hasUser ?user. ?class rdfs:subClassOf sec:Clothing.'+
  								  '?item rdf:type ?class. ?wardrobe sec:contains ?item. ?item sec:url ?url. '+
  								  '?item sec:description ?description. ?item sec:size ?size. ?item rdf:type ?category.'+
  								  ' ?category rdf:subClassOf* sec:'+ category +' .'+
  								  '?item sec:colour ?c. ?c rdf:type ?colour ' +
                                  'FILTER(?category != owl:NamedIndividual && ?colour != owl:NamedIndividual)}';
 
    var jsonData = {};

    stardogFunctions.queryDB( queryClothingWithType,function (result) {
		jsonData["user-clothing-"+category] = processResponse(result);
    	callback(jsonData);
    });
}

// /wardrobe/shoes/shoes_category
exports.findShoesWithCategoryForUser = function(username,category,callback){
	var queryShoesWithCategory = prefixs + 'select ?item ?category ?colour ?url ?size ?description { ?user rdf:type sec:User. ?user sec:username \"' + username + '\" .' +
  								  '?wardrobe sec:hasUser ?user. ?class rdfs:subClassOf sec:Shoes.'+
  								  '?item rdf:type ?class. ?wardrobe sec:contains ?item. ?item sec:url ?url. '+
  								  '?item sec:description ?description. ?item sec:size ?size. ?item rdf:type ?category. '+
  								  ' ?category rdf:subClassOf* sec:'+ category +' .'+
  								  '?item sec:colour ?c. ?c rdf:type ?colour ' +
                                  'FILTER(?category != owl:NamedIndividual && ?colour != owl:NamedIndividual)}';

    var jsonData = {};

    stardogFunctions.queryDB( queryShoesWithCategory,function (result) {
		jsonData["user-shoes-"+category] = processResponse(result);
    	callback(jsonData);
    });
}

var processResponse = function(result){
	 var itemsArray = [];
	  for(var i=0;i<result.length;i++){
       	 var item = {};
         item["name"] =  result[i].item.value.split("#").pop();
         item["category"] = result[i].category.value.split("#").pop();
         item["colour"] = result[i].colour.value.split("#").pop();
         item["url"] = result[i].url.value;
         item["size"] =  result[i].size.value;
         item["description"] =  result[i].description.value;
       	 itemsArray.push(item);
	   }
	return itemsArray;
}