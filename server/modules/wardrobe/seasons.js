var stardogFunctions = require("../stardog-functions.js");

var prefixs = 'PREFIX sec:<http://students.info.uaic.ro/~madalina.vatamanelu/wade/sec.owl#>'+
			  'PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>'+
			  'PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>'+
			  'PREFIX owl:<http://www.w3.org/2002/07/owl#>';

var querySeasons = prefixs +
				'select distinct ?season { ?season rdfs:subClassOf sec:Season. }';

exports.getAllSeasons = function(callback){
	var jsonData = {};
    var seasonsArray = [];

    stardogFunctions.queryDB( querySeasons, function (result) {
        for(var i=0;i<result.length;i++){
       	 var lastPart = result[i].season.value.split("#").pop();
       	 seasonsArray.push(lastPart);
		}
		jsonData["seasons"]=seasonsArray;
    	callback(jsonData);
    });
}