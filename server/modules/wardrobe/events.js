var stardogFunctions = require("../stardog-functions.js");

var prefixs = 'PREFIX sec:<http://students.info.uaic.ro/~madalina.vatamanelu/wade/sec.owl#>'+
			  'PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>'+
			  'PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>'+
			  'PREFIX owl:<http://www.w3.org/2002/07/owl#>';

var queryEvents = prefixs +
				'select distinct ?event { ?event rdfs:subClassOf* sec:Event. }';

var SELECT_PREFIX = 'select distinct ?item ?category ?colour ?url ?size ?description ';
var PREFIX_USER = ' { ?user rdf:type sec:User. ?user sec:username \"';
var SUFFIX_USER = '\" . ?wardrobe sec:hasUser ?user.';
var PREFIX_EVENT ='?propEv a owl:ObjectProperty.   ?propEv rdfs:domain ?domain. ?propEv rdfs:range sec:';
var PREFIX_STYLE = ' ?propSt a owl:ObjectProperty. ?propSt rdfs:range ?domain. ?propSt rdfs:domain sec:';
var PREFIX_SEASON = '?propSe a owl:ObjectProperty. ?propSe rdfs:range ?domain. ?propSe rdfs:domain sec:';
var PREFIX_SHOES = ' ?propShoes a owl:ObjectProperty. ?propShoes rdfs:range ?domain. ?propShoes rdfs:domain ?domainShoes.';
var SUFFIX_WARDROBE = ' ?wardrobe sec:contains ?item. ?item sec:url ?url. ?item sec:description ?description. '+
					  ' ?item sec:size ?size. ?item rdf:type ?category. ?item sec:colour ?c. ?c rdf:type ?colour '+
                      ' FILTER(?category != owl:NamedIndividual && ?colour != owl:NamedIndividual';


// /events
exports.getAllEvents = function(callback){
	var jsonData = {};
    var eventsArray = [];

    stardogFunctions.queryDB( queryEvents, function (result) {
        for(var i=0;i<result.length;i++){
       	 var lastPart = result[i].event.value.split("#").pop();
       	 eventsArray.push(lastPart);
		}
		jsonData["events"]=eventsArray;
    	callback(jsonData);
    });
}

////////////////////////// clothing + shoes////////////////////////////////////////
// /events/event_name/
exports.findItemsForEvent = function(username,eventType,callback){
	var queryItemsForEvent = prefixs + SELECT_PREFIX  + '{' + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
							'?item rdf:type ?domain.'+ SUFFIX_WARDROBE + ')}' +
                             'UNION'+
                            PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + PREFIX_SHOES +
							'?item rdf:type ?domainShoes.'+ SUFFIX_WARDROBE  +')}}';

    var jsonData = {};

    stardogFunctions.queryDB( queryItemsForEvent,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name&colour=colour_name
exports.findItemsForEventAndColour = function(username,eventType,colour,callback){
	var querytemsForEventAndColour =  prefixs + SELECT_PREFIX  + '{' + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
							'?item rdf:type ?domain.'+ SUFFIX_WARDROBE + ' && ?colour = sec:'+ colour +')}'+
                            'UNION'+
                             PREFIX_USER + username + SUFFIX_USER +
	                         PREFIX_EVENT + eventType + "." + PREFIX_SHOES +
							'?item rdf:type ?domainShoes.'+ SUFFIX_WARDROBE  + ' && ?colour = sec:'+ colour  +')}}';

    var jsonData = {};

    stardogFunctions.queryDB( querytemsForEventAndColour,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name&style=style_name
exports.findItemsForEventAndStyle = function(username,eventType,style,callback){
	var queryItemsForEventAndStyle = prefixs + SELECT_PREFIX  + '{' + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_STYLE + style + "." +
							'?item rdf:type ?domain.'+ SUFFIX_WARDROBE + ')}'+
 							'UNION'+
 							PREFIX_USER + username + SUFFIX_USER +
 							PREFIX_EVENT + eventType + "." + 
	                        PREFIX_STYLE + style + "." +  PREFIX_SHOES +
	                        '?item rdf:type ?domainShoes.'+ SUFFIX_WARDROBE  + ')}}';


    var jsonData = {};

    stardogFunctions.queryDB( queryItemsForEventAndStyle,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name&season=season_name
exports.findItemsForEventAndSeason = function(username,eventType,season,callback){
	var queryItemsForEventAndSeason =  prefixs + SELECT_PREFIX + '{'+ PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_SEASON + season + "." +
							'?item rdf:type ?domain.'+ SUFFIX_WARDROBE + ')}'+
						    'UNION'+
						    PREFIX_USER + username + SUFFIX_USER +
						    PREFIX_EVENT + eventType + "." + 
	                        PREFIX_SEASON + season + "." + PREFIX_SHOES +
	                        '?item rdf:type ?domainShoes.'+ SUFFIX_WARDROBE  + ')}}';


    var jsonData = {};

    stardogFunctions.queryDB( queryItemsForEventAndSeason,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name&style=style_name&season=season_name
exports.findItemsForEventAndStyleAndSeason = function(username,eventType,style,season,callback){
	var queryItemsForEventAndSeason =  prefixs + SELECT_PREFIX + '{' + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_STYLE + style + "." +
	                        PREFIX_SEASON + season + "." +
							'?item rdf:type ?domain.'+ SUFFIX_WARDROBE + ')}'+
							'UNION'+
							PREFIX_USER + username + SUFFIX_USER +
							PREFIX_EVENT + eventType + "." + 
	                        PREFIX_STYLE + style + "." +
	                        PREFIX_SEASON + season + "." + PREFIX_SHOES +
	                        '?item rdf:type ?domainShoes.'+ SUFFIX_WARDROBE  + ')}}';

    var jsonData = {};

    stardogFunctions.queryDB( queryItemsForEventAndSeason,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name&style=style_name&colour=colour_name
exports.findItemsForEventAndStyleAndColour = function(username,eventType,style,colour,callback){
	var queryItemsForEventAndSeason =  prefixs + SELECT_PREFIX + '{' + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_STYLE + style + "." +
							'?item rdf:type ?domain.'+ SUFFIX_WARDROBE + ' && ?colour = sec:'+ colour +')}'+
							'UNION'+
							PREFIX_USER + username + SUFFIX_USER +
							PREFIX_EVENT + eventType + "." + 
	                        PREFIX_STYLE + style + "." + PREFIX_SHOES +
	                        '?item rdf:type ?domainShoes.'+ SUFFIX_WARDROBE + ' && ?colour = sec:'+ colour  + ')}}';

    var jsonData = {};

    stardogFunctions.queryDB( queryItemsForEventAndSeason,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name&season=season_name&colour=colour_name
exports.findItemsForEventAndSeasonAndColour = function(username,eventType,season,colour,callback){
	var queryItemsForEventAndSeason =  prefixs + SELECT_PREFIX + '{' + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_SEASON + season + "." +
							'?item rdf:type ?domain.'+ SUFFIX_WARDROBE + ' && ?colour = sec:'+ colour +')}'+
							'UNION'+
							PREFIX_USER + username + SUFFIX_USER +
							PREFIX_EVENT + eventType + "." + 
	                        PREFIX_SEASON + season + "." + PREFIX_SHOES +
	                        '?item rdf:type ?domainShoes.'+ SUFFIX_WARDROBE + ' && ?colour = sec:'+ colour  + ')}}';

    var jsonData = {};

    stardogFunctions.queryDB( queryItemsForEventAndSeason,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name&style=style_name&season=season_name&colour=colour_name
exports.findItemsForEventAndStyleAndSeasonAndColour = function(username,eventType,style,season,colour,callback){
	var queryItemsForEventAndSeason =  prefixs + SELECT_PREFIX + '{' + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_STYLE + style + "." +
	                        PREFIX_SEASON + season + "." +
							'?item rdf:type ?domain.'+ SUFFIX_WARDROBE + ' && ?colour = sec:'+ colour  + ')}'+
							'UNION'+
							PREFIX_USER + username + SUFFIX_USER +
							PREFIX_EVENT + eventType + "." + 
	                        PREFIX_STYLE + style + "." +
	                        PREFIX_SEASON + season + "." + PREFIX_SHOES +
	                        '?item rdf:type ?domainShoes.'+ SUFFIX_WARDROBE + ' && ?colour = sec:'+ colour  + ')}}';

    var jsonData = {};

    stardogFunctions.queryDB( queryItemsForEventAndSeason,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

////////////////////////// clothing ////////////////////////////////////////
// /events/event_name/clothing
exports.findClothingForEvent = function(username,eventType,callback){
	var queryClothingForEvent = prefixs + SELECT_PREFIX + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
							'?item rdf:type ?domain.'+ SUFFIX_WARDROBE + ')}';

    var jsonData = {};

    stardogFunctions.queryDB( queryClothingForEvent,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name/clothing&colour=colour_name
exports.findClothingForEventAndColour = function(username,eventType,colour,callback){
	var queryClothingForEventAndColour =  prefixs + SELECT_PREFIX + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
							'?item rdf:type ?domain.'+ SUFFIX_WARDROBE + ' && ?colour = sec:'+ colour +')}';

    var jsonData = {};

    stardogFunctions.queryDB( queryClothingForEventAndColour,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name/clothing&style=style_name
exports.findClothingForEventAndStyle = function(username,eventType,style,callback){
	var queryClothingForEventAndStyle = prefixs + SELECT_PREFIX + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_STYLE + style + "." +
							'?item rdf:type ?domain.'+ SUFFIX_WARDROBE + ')}';

    var jsonData = {};

    stardogFunctions.queryDB( queryClothingForEventAndStyle,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name/clothing&season=season_name
exports.findClothingForEventAndSeason = function(username,eventType,season,callback){
	var queryClothingForEventAndSeason =  prefixs + SELECT_PREFIX + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_SEASON + season + "." +
							'?item rdf:type ?domain.'+ SUFFIX_WARDROBE + ')}';

    var jsonData = {};

    stardogFunctions.queryDB( queryClothingForEventAndSeason,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name/clothing&style=style_name&season=season_name
exports.findClothingForEventAndStyleAndSeason = function(username,eventType,style,season,callback){
	var queryClothingForEventAndSeason =  prefixs + SELECT_PREFIX + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_STYLE + style + "." +
	                        PREFIX_SEASON + season + "." +
							'?item rdf:type ?domain.'+ SUFFIX_WARDROBE + ')}';

    var jsonData = {};

    stardogFunctions.queryDB( queryClothingForEventAndSeason,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name/clothing&style=style_name&colour=colour_name
exports.findClothingForEventAndStyleAndColour = function(username,eventType,style,colour,callback){
	var queryClothingForEventAndSeason =  prefixs + SELECT_PREFIX + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_STYLE + style + "." +
							'?item rdf:type ?domain.'+ SUFFIX_WARDROBE + ' && ?colour = sec:'+ colour +')}';

    var jsonData = {};

    stardogFunctions.queryDB( queryClothingForEventAndSeason,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name/clothing&season=season_name&colour=colour_name
exports.findClothingForEventAndSeasonAndColour = function(username,eventType,season,colour,callback){
	var queryClothingForEventAndSeason =  prefixs + SELECT_PREFIX + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_SEASON + season + "." +
							'?item rdf:type ?domain.'+ SUFFIX_WARDROBE + ' && ?colour = sec:'+ colour +')}';

    var jsonData = {};

    stardogFunctions.queryDB( queryClothingForEventAndSeason,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name/clothing&style=style_name&season=season_name&colour=colour_name
exports.findClothingForEventAndStyleAndSeasonAndColour = function(username,eventType,style,season,colour,callback){
	var queryClothingForEventAndSeason =  prefixs + SELECT_PREFIX + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_STYLE + style + "." +
	                        PREFIX_SEASON + season + "." +
							'?item rdf:type ?domain.'+ SUFFIX_WARDROBE + ' && ?colour = sec:'+ colour +')}';

    var jsonData = {};

    stardogFunctions.queryDB( queryClothingForEventAndSeason,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

////////////////////////// shoes////////////////////////////////////////
// /events/event_name/shoes
exports.findShoesForEvent = function(username,eventType,callback){
	var queryShoesForEvent = prefixs + SELECT_PREFIX + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + PREFIX_SHOES + 
							'?item rdf:type ?domainShoes.'+ SUFFIX_WARDROBE + ')}';

    var jsonData = {};

    stardogFunctions.queryDB( queryShoesForEvent,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name/shoes&colour=colour_name
exports.findShoesForEventAndColour = function(username,eventType,colour,callback){
	var querytemsForEventAndColour =  prefixs + SELECT_PREFIX + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." +  PREFIX_SHOES +
							'?item rdf:type ?domainShoes.'+ SUFFIX_WARDROBE + ' && ?colour = sec:'+ colour +')}';

    var jsonData = {};

    stardogFunctions.queryDB( querytemsForEventAndColour,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name/shoes&style=style_name
exports.findShoesForEventAndStyle = function(username,eventType,style,callback){
	var queryShoesForEventAndStyle = prefixs + SELECT_PREFIX + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_STYLE + style + "."  +  PREFIX_SHOES +
							'?item rdf:type ?domainShoes.'+ SUFFIX_WARDROBE + ')}';

    var jsonData = {};

    stardogFunctions.queryDB( queryShoesForEventAndStyle,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name/shoes&season=season_name
exports.findShoesForEventAndSeason = function(username,eventType,season,callback){
	var queryShoesForEventAndSeason =  prefixs + SELECT_PREFIX + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_SEASON + season + "."  +  PREFIX_SHOES +
							'?item rdf:type ?domainShoes.'+ SUFFIX_WARDROBE + ')}';

    var jsonData = {};

    stardogFunctions.queryDB( queryShoesForEventAndSeason,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name/shoes&style=style_name&season=season_name
exports.findShoesForEventAndStyleAndSeason = function(username,eventType,style,season,callback){
	var queryShoesForEventAndSeason =  prefixs + SELECT_PREFIX + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_STYLE + style + "." +
	                        PREFIX_SEASON + season + "."  +  PREFIX_SHOES +
							'?item rdf:type ?domainShoes.'+ SUFFIX_WARDROBE + ')}';

    var jsonData = {};

    stardogFunctions.queryDB( queryShoesForEventAndSeason,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name/shoes&style=style_name&colour=colour_name
exports.findShoesForEventAndStyleAndColour = function(username,eventType,style,colour,callback){
	var queryShoesForEventAndSeason =  prefixs + SELECT_PREFIX + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_STYLE + style + "."  +  PREFIX_SHOES +
							'?item rdf:type ?domainShoes.'+ SUFFIX_WARDROBE + ' && ?colour = sec:'+ colour +')}';

    var jsonData = {};

    stardogFunctions.queryDB( queryShoesForEventAndSeason,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name/shoes&season=season_name&colour=colour_name
exports.findShoesForEventAndSeasonAndColour = function(username,eventType,season,colour,callback){
	var queryShoesForEventAndSeason =  prefixs + SELECT_PREFIX + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_SEASON + season + "."  +  PREFIX_SHOES +
							'?item rdf:type ?domainShoes.'+ SUFFIX_WARDROBE + ' && ?colour = sec:'+ colour +')}';

    var jsonData = {};

    stardogFunctions.queryDB( queryShoesForEventAndSeason,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}

// /events/event_name/shoes&style_style_name&season=season_name&colour=colour_name
exports.findShoesForEventAndStyleAndSeasonAndColour = function(username,eventType,style,season,colour,callback){
	var queryShoesForEventAndSeason =  prefixs + SELECT_PREFIX + PREFIX_USER + username + SUFFIX_USER +
	                        PREFIX_EVENT + eventType + "." + 
	                        PREFIX_STYLE + style + "."  +
	                        PREFIX_SEASON + season + "."  +  PREFIX_SHOES +
							'?item rdf:type ?domainShoes.'+ SUFFIX_WARDROBE + ' && ?colour = sec:'+ colour +')}';

    var jsonData = {};

    stardogFunctions.queryDB( queryShoesForEventAndSeason,function (result) {
		jsonData["response"] = processResponse(result);
    	callback(jsonData);
    });
}


var processResponse = function(result){
	 var itemsArray = [];
	  for(var i=0;i<result.length;i++){
       	 var item = {};
         item["name"] =  result[i].item.value.split("#").pop();
         item["category"] = result[i].category.value.split("#").pop();
         item["colour"] = result[i].colour.value.split("#").pop();
         item["url"] = result[i].url.value;
         item["size"] =  result[i].size.value;
         item["description"] =  result[i].description.value;
       	 itemsArray.push(item);
	   }
	return itemsArray;
}