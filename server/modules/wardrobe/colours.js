var stardogFunctions = require("../stardog-functions.js");

var prefixs = 'PREFIX sec:<http://students.info.uaic.ro/~madalina.vatamanelu/wade/sec.owl#>'+
			  'PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>'+
			  'PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>'+
			  'PREFIX owl:<http://www.w3.org/2002/07/owl#>';

var queryColours = prefixs +
				'select distinct ?colour { ?colour rdfs:subClassOf sec:Colour. }';

exports.getAllColours = function(callback){
	var jsonData = {};
    var coloursArray = [];

    stardogFunctions.queryDB( queryColours, function (result) {
        for(var i=0;i<result.length;i++){
       	 var lastPart = result[i].colour.value.split("#").pop();
       	 coloursArray.push(lastPart);
		}
		jsonData["colours"]=coloursArray;
    	callback(jsonData);
    });
}